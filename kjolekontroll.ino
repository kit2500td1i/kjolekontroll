#include <OneWire.h>
#include <DallasTemperature.h>

// Hvor mange sensorer som er koblet til
#define ANTALL_SENSORER 4

// Hvilken vi skal regulere etter
#define SENSOR_NR 0

// Ønsket midtpunkt
#define TEMPERATUR 25.0

// Høyere temperatur enn dette antas å være feil
#define TEMPERATUR_FEIL 30.0

// Ønsket hysterese: Får området fra (TEMPERATUR-HYSTERESE) til (TEMPERATUR+HYSTERESE)
#define HYSTERESE 1.0

// Sett hvilken port som skal brukes for temperatursensor
#define ONE_WIRE_BUS 2

// Sett hvilken port som skal brukes for rele
#define RELE 3

// Sett hvilken port som skal brukes for lysdiode
#define LYSDIODE 4

// For å holde oversikt over tilstander
typedef enum OnOffStateTag
{
	AV = 0, PAA = 1
} OnOffState_t;

// For å holde oversikt over hvilke tilstander programmet kan være i
typedef enum ProgramTilstandTag
{
	NORMAL = 0, FEIL = 1
} ProgramTilstand_t;

OneWire oneWire(ONE_WIRE_BUS); // Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas ICs)
DallasTemperature sensors(&oneWire); // Pass our oneWire reference to Dallas Temperature. 
ProgramTilstand_t programTilstand = NORMAL; // Tilstand, om alt er i orden eller ikke
OnOffState_t lysdiodeTilstand = AV; // Tilstand, om lysdiode er av eller på
OnOffState_t kjoleState = AV; // Tilstand, om rele er av eller på

unsigned long lysdiodeTimer; // timer for håndtering av lysdiodeblinking
unsigned long minuttTimer; // timer for hvert 60 sekund
unsigned long minuttTeller = 0; // antall minutter som har gått siden start
unsigned long hviletimer = 0; // Antall minutter den har kjørt eller ikke kjørt..
float temperatur[ANTALL_SENSORER]; // Avlest temperatur i Celcius

// the setup routine runs once when you press reset:
void setup()
{
	// initialize serial communication at 9600 bits per second:
	Serial.begin(9600);

	// Start up the library
	sensors.begin();

	// Sett rele som utgang
	pinMode(RELE, OUTPUT);

	// Sett lysdiode som utgang
	pinMode(LYSDIODE, OUTPUT);

	// Skru lysdiode på
	digitalWrite(LYSDIODE, HIGH);

	// Vent 1 sekund
	delay(1000);

	// Skru lysdiode av
	digitalWrite(LYSDIODE, LOW);

	// Start timere
	lysdiodeTimer = minuttTimer = millis();

}

// Blink med lysdioden
// periodeTid i ms
// paa Hvor lenge lysdioden vil være på i ms
void blinkLysdiode(unsigned int periodeTid, unsigned int paa)
{
	switch (lysdiodeTilstand) {
	case PAA:
		if (millis() - lysdiodeTimer >= (paa - 1)) {
			lysdiodeTimer = millis(); // nullstill timer
			digitalWrite(LYSDIODE, LOW); // skru av
			lysdiodeTilstand = AV; // hopp til av tilstand
		}
		break;
	case AV:
		if (millis() - lysdiodeTimer >= (periodeTid - paa - 1)) {
			lysdiodeTimer = millis(); // nullstill timer
			digitalWrite(LYSDIODE, HIGH); // skru på
			lysdiodeTilstand = PAA; // hopp til på tilstand
		}
		break;
	}
}

// Skriv temperatur og om kjøling foregår til serieport
void skrivTilSerieport(void)
{
	Serial.print(minuttTeller);
        for(int i = 0; i < ANTALL_SENSORER; i++) {
	        Serial.print("\t");
        	Serial.print(temperatur[i]);
        }
	Serial.print("\t");
	Serial.print(kjoleState);
	Serial.print("\t");
	Serial.println(hviletimer);
	minuttTeller++;
        hviletimer++;
}

// Implementer hysterese basert på temperatur
void hystereseStyring(void)
{
	switch (kjoleState) {
	case AV: // Hvis AV: // Er temperatur høyere enn grense?
		if (temperatur[SENSOR_NR] > (TEMPERATUR + HYSTERESE)) {
			digitalWrite(RELE, HIGH); // Skru på rele
			digitalWrite(LYSDIODE, HIGH); // Skru på lysdiode
                        lysdiodeTilstand = PAA; // oppdater tilstand for lysdiode
			kjoleState = PAA; // Gå til tilstand på
                        hviletimer = 0;
		}
		break;
	case PAA: // Hvis PÅ: Er temperatur laverne enn grense?
		if (temperatur[SENSOR_NR] < (TEMPERATUR - HYSTERESE)) {
			digitalWrite(RELE, LOW); // Skru av rele
			digitalWrite(LYSDIODE, LOW); // Skru av lysdiode
                        lysdiodeTilstand = AV; // oppdater tilstand for lysdiode
                        lysdiodeTimer = millis(); // nullstill timer
			kjoleState = AV; // Gå til tilstand av
                        hviletimer = 0;
		}
		break;
	default:
		break;
	}
}

// Reguler temperatur
void regulerTemperatur(void)
{
	// 60 sekunds timer
	if (millis() - minuttTimer >= 5999) {
		minuttTimer = millis(); // nullstill timer
		sensors.requestTemperatures(); // Send the command to get temperaturs
                for(int i = 0; i < ANTALL_SENSORER; i++) {
		        temperatur[i] = sensors.getTempCByIndex(i); // Les ut temperatur
                }

     
                // temperaturbasert hysteresestyring
		hystereseStyring();

		skrivTilSerieport();

		// Sjekk hvis overtemperatur og isåfall steng ned (feilsituasjon)
		if (temperatur[SENSOR_NR] > TEMPERATUR_FEIL) {
			programTilstand = FEIL;
                        kjoleState = AV;
                        lysdiodeTimer = millis(); // nullstill timer
		}
	}
}

// Normal tilstand for programmet
void NormalTilstand(void)
{
	// reguler temperatur med termostat
	regulerTemperatur();

        if(kjoleState == AV) { // kun blinking når kjøling er av
               	// Håndter lysdiode blinking. 1 sekund periode tid og 100 ms på.
                blinkLysdiode(2000, 100);
        }
}

// Dersom feil oppstår
void FeilTilstand(void)
{
//        Serial.write('F');
	digitalWrite(RELE, LOW); // Skru av rele

	blinkLysdiode(500, 250); // blink med en frekvens på 2 Hz
}

// the loop routine runs over and over again forever:
void loop()
{
	switch (programTilstand) {
	case NORMAL:
		NormalTilstand();
		break;
	case FEIL:
		FeilTilstand();
		break;
	default:
		break;
	}
}

